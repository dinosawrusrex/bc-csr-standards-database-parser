# BC CSR Standards Viewer

This repository will eventually host code for a viewer of the standards found in the Schedules of the [Contaminated Sites Regulation (CSR) document](http://www.bclaws.ca/civix/document/id/crbc/crbc/375_96_multi) provided by the British Columbia provincial government of Canada. The schedules of interest are from 3.1 to 3.3. Depending on the chemical and three factors (land use, site-specific factor, and other relevant conditions such as pH or water hardness), there are standards (concentration limits) set in place. Currently, the format to view these standards is error-prone and tedious. So, the goal of this viewer is to make obtaining these standards robust and quicker. An additional goal is to have something that can automatically compare the standards with lab sample data, and the code for this project may be reusable for this additional goal.


## Stages

- [x] Convert PDF table into a spreadsheet
	- Converted on https://smallpdf.com/pdf-to-excel
- [ ] Migrate data from spreadsheet into database
	- [x] Schedule 3.1 Part 1
	- [ ] Remaining of schedule 3.1, schedule 3.2, and schedule 3.3
- [ ] Build viewer to call data from database given the chemical, land use, and site factors of interest
	- [x] Build mockup
	- [ ] Improve looks, user interface, and user experience

## How to use this program
Note: Windows Users, use backslashes instead of forward slashes when running files in different directories!

### Installing Python 3

There are currently two versions of Python: Python 2 and Python 3. The code is written mostly using syntax from 3 so it is best to use 3.

Visit [https://www.python.org/downloads/](https://www.python.org/downloads/) to download the most recent version of Python 3 for your operating system.

When installing Python on Windows, make sure to check of allowing Windows to add Python to its path, and to allow Python to override the length limit when executing scripts.

After installing, on your command-line interface (CLI), ensure it is Python 3 that is installed:
```python
python --version
# output should be Python 3.x.x
```

If you get the error `'python' is not recognized as an internal or external command, operable program or batch file.`, use `py` in place of `python`:
```python
py --version
# output should be Python 3.x.x
```

### Download the program

Download this repository. Button is under `Clone` and next to `Web IDE`. Unzip the content into your desired directory, perhaps the Desktop for ease.

Navigate to the root of the repository _i.e._ the directory that contains the `parser` and `web_viewer` directories.

### Set up a virtual environment

Python has something called virtual environments. Often, applications may need to import other modules, and it is nice to not clutter your system with these modules. Also, you may want specific versions of modules, and virtual environments make this easier to handle. Python 3 automatically comes with dealings with virtual environments [https://docs.python.org/3/tutorial/venv.html](https://docs.python.org/3/tutorial/venv.html).

On your CLI, you should already be in the root of the repository. If not, navigate to it.

Make a new virtual environment, which will create a directory named `csr-viewer` in here:

`python -m venv csr-viewer`

Activate the virtual environment:

For Windows:

`csr-viewer\Scripts\activate.bat`

For Linux:

`source csr-viewer/bin/activate`

### Install the requirements for this project

`pip install -r requirements.txt`

Python may ask you to upgrade pip (its default package manager). Follow the instructions.

### Play around!

Checkout the [web viewer directory](/web_viewer/) that is built with [Flask](http://flask.pocoo.org/). It is still very much work-in-progress, but working towards interactivity, making it look nice, and displaying information that is relevant.

Checkout the [parser directory](/parser/). The parser for Schedule 3.1 Part 1 is built, and the others are still to be built.
