# Database Design

## Land Use Concentration Limit
uuid | land_use | site_specific_factor | chemical | value | unit | date | pH min | pH max | h2o_hard_min | h20_hard_max | limit_type
--- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | ---
1 | wln | human_intake | nickel | 900 | ug/g | 2019-04-09 | None | None | None | None | max

## Chemical
name | CAS | schedule | matrix
--- | --- | --- | ---
nickel | 7440-02-0 | 3.1 | 24

## Land Use
code | name
--- | ---
wln | wildlands natural
wlr | wildlands reverted
al | agricultural
pl | urban park
rlld | residential low density
rlhd | residential high density
cl | commercial
il | industrial
aw | aquatic life
iw | irrigation
lw | livestock
dw | drinking water
p | parkade

## Site Specific Factor
code | factor | category
--- | --- | ---
human_intake | Intake of contaminated soil | Human Health Protection
human_drinking | Groundwater used for drinking water | Human Health Protection
inverts_plants | Toxicity to soil invertebrates and plants | Environmental Protection
livestock_ingestion | Livestock ingesting soil and fodder | Environmental Protection
microbial | Major microbial functional impairment | Environmental Protection
groundwater_flow | Groundwater flow to surface water used by aquatic life | Environmental Protection
freshwater_flow | Groundwater flow to surface water used by aquatic life (freshwater) | Environmental Protection
marine_flow | Groundwater flow to surface water used by aquatic life (marine) | Environmental Protection
watering | Groundwater used for livestock watering | Environmental Protection
irrigation | Groundwater used for irrigation | Environmental Protection
