import datetime
import re


def land_use_concentration_limit_arguments(raw_value, land_use, row_dict, chemical):
    return (
        land_use,
        site_factor(row_dict),
        chemical,
        value(raw_value),
        unit(raw_value),
        datetime.date(2019, 5, 5),
        ph_min(row_dict),
        ph_max(row_dict),
        limit_type(raw_value),
    )


def value(raw_value):
    if raw_value in [None, "NS"]:
        return None
    elif isinstance(raw_value, int):
        return float(raw_value)
    elif isinstance(raw_value, float):
        return raw_value
    elif re.findall(r"\d+\s+\d+", raw_value):
        return float(re.findall(r"\d+\s+\d+", raw_value)[0].replace(" ", ""))
    else:
        return float(re.findall(r"\d*\.?\d+", raw_value)[0])


def unit(raw_value):
    if raw_value in [None, "NS"]:
        return None
    elif isinstance(raw_value, str) and re.findall(r"\d+\s*(\w+\/\w+)", raw_value):
        return re.findall(r"\d+\s*(\w+\/\w+)", raw_value)[0]
    else:
        return "ug/g"


def limit_type(raw_value):
    if raw_value in [None, "NS"]:
        return None
    elif isinstance(raw_value, str) and ">" in raw_value:
        return "min"
    else:
        return "max"


def site_factor(row_dict):
    return row_dict["site_factor"]


def ph_min(row_dict):
    if "ph_min" in row_dict:
        return row_dict["ph_min"]
    else:
        return None


def ph_max(row_dict):
    if "ph_max" in row_dict:
        return row_dict["ph_max"]
    else:
        return None
