import sqlite3


def initialise_database(database_file):
    with sqlite3.connect(database_file) as connection:  # auto commit and close
        cursor = connection.cursor()
        try:
            cursor.execute("select null from land_use, site_specific_factor limit 1;")
        except Exception:
            create_database(cursor)


def create_database(cursor):
    create_land_use_concentration_limit_table(cursor)
    create_chemical_table(cursor)
    create_and_populate_site_specific_factor_table(cursor)
    create_and_populate_land_use_table(cursor)


def create_land_use_concentration_limit_table(cursor):
    cursor.execute(
        """ CREATE TABLE land_use_concentration_limit (
                    id integer PRIMARY KEY,
                    land_use text NOT NULL,
                    site_specific_factor text NOT NULL,
                    chemical text NOT NULL,
                    value real,
                    unit text,
                    date date,
                    ph_min real,
                    ph_max real,
                    h2o_hard_min real,
                    h20_hard_max real,
                    limit_type text); """
    )


def create_chemical_table(cursor):
    cursor.execute(
        """ CREATE TABLE chemical (
                    name text PRIMARY KEY,
                    cas text,
                    schedule real,
                    matrix integer); """
    )


def create_and_populate_land_use_table(cursor):
    cursor.execute(
        """ CREATE TABLE land_use (
                        code text PRIMARY KEY,
                        name text); """
    )
    cursor.executemany(
        """ INSERT INTO land_use (code, name)
        VALUES (?, ?);""",
        [
            ("wln", "wildlands natural"),
            ("wlr", "wildlands reverted"),
            ("al", "agricultural"),
            ("pl", "urban park"),
            ("rlld", "residential low density"),
            ("rlhd", "residential high density"),
            ("cl", "commercial"),
            ("il", "industrial"),
            ("aw", "aquatic life"),
            ("iw", "irrigation"),
            ("lw", "livestock"),
            ("dw", "drinking water"),
            ("p", "parkade"),
        ],
    )


def create_and_populate_site_specific_factor_table(cursor):
    cursor.execute(
        """CREATE TABLE site_specific_factor (
                        code text PRIMARY KEY,
                        factor text,
                        category text);"""
    )
    cursor.executemany(
        """INSERT INTO site_specific_factor
        (code, factor, category) VALUES (?, ?, ?);""",
        [
            ("human_intake", "Intake of contaminated soil", "Human Health Protection"),
            (
                "human_drinking",
                "Groundwater used for drinking water",
                "Human Health Protection",
            ),
            (
                "inverts_plants",
                "Toxicity to soil invertebrates and plants",
                "Environmental Protection",
            ),
            (
                "livestock_ingestion",
                "Livestock ingesting soil and fodder",
                "Environmental Protection",
            ),
            (
                "microbial",
                "Major microbial functional impairment",
                "Environmental Protection",
            ),
            (
                "groundwater_flow",
                "Groundwater flow to surface water used by aquatic life",
                "Environmental Protection",
            ),
            (
                "freshwater_flow",
                "Groundwater flow to surface water used by aquatic life freshwater)",
                "Environmental Protection",
            ),
            (
                "marine_flow",
                "Groundwater flow to surface water used by aquatic life marine)",
                "Environmental Protection",
            ),
            (
                "irrigation",
                "Groundwater used for irrigation",
                "Environmental Protection",
            ),
        ],
    )


def populate_chemical_table(database_file, arguments):
    with sqlite3.connect(database_file) as connection:
        cursor = connection.cursor()
        cursor.execute(
            """INSERT INTO chemical(name, CAS, schedule, matrix)
        VALUES (?, ?, ?, ?);""",
            arguments,
        )


def populate_land_use_concentration_limit_table(database_file, arguments):
    with sqlite3.connect(database_file) as connection:
        cursor = connection.cursor()
        cursor.executemany(
            """INSERT INTO land_use_concentration_limit
        (land_use, site_specific_factor, chemical, value,
        unit, date, ph_min, ph_max, limit_type)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);""",
            arguments,
        )
