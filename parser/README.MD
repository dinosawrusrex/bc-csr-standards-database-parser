# Parser Readme

## Sample Usage

I will eventually put together a ZIP of the Excel spreadsheets used, but for now, here is a sample of how to use this code. This parser is using the test_csr.xlsx which contains the table for Anthracene.

```python
python sample_run_program.py
# windows: python sample_run_program.py
# should output ('anthracene', '120-12-7', 3.1, 1)
```
## Call on a specific standard

Currently the sample program is showing what is in the chemical table (which is just Anthracene). Let's query the database to find out specific concentration limits, which is way more interesting!

Open `sample_run_program.py` for editing.

Navigate to the line containing `def test_sql_queries(database_file):` To briefly explain, this is a function where I have decided we can input SQL queries to access values within the database.

The database contains four tables linked together:
1. land_use_concentration_limit
	- the main table containing all the standards
	- foreign key links to land_use, site_specific_factor, and chemical
2. chemical
	- contains details about the chemical including cas, matrix, and schedule
3. site_specific_factor
4. land_use

Currently, the query is `select * from chemical` where we are selecting all the values from the chemical table.

Let's try and view the standards. Let's say we want to find all values for `anthracene` with land use of residential low-density (`rlld`). We remove `select * from chemical` and replace it with the following:
```sql
select site_factor, value from land_use_concentration_limit
where chemical = "anthracene"
and land_use = "rlld";
```
Save the file and re-run `python parser/sample_run_program.py`. The output should be be a list of site specific factors and their corresponding values.

Note, I have built the program to delete the database every time the program finishes running, for testing purposes. However, Python in Windows does not perform this properly, so Windows users might have to manually delete the database file if the program fails. The database is named 'land_use_conc_limit.db'.

I will add more to this section soon to explain the tables in more depth. Or you may checkout the database_design file in the [docs](../docs) directory.
